const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const port = 8000;
app.use(express.json());


app.post('/encode', (req, res) => {
    const text = Vigenere.Cipher(req.body.password).crypt(req.body.message);

    res.send({'encoded': text});
});

app.post('/decode', (req, res) => {
    const text = Vigenere.Decipher(req.body.password).crypt(req.body.message);

    res.send({'decoded': text});
});


app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});